resource "aws_iam_group" "developers" {
  name = "tc-lab-dev"
}

resource "aws_iam_group_policy" "DeveloperPolicy" {
  name  = "DeveloperPolicy"
  group = "${aws_iam_group.developers.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sts:AssumeRole",
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.s3library.arn}"
    }
  ]
}
EOF
}

resource "aws_iam_role" "tc-lab-dev" {
  name = "tc-lab-dev"

  assume_role_policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}

resource "aws_iam_policy" "tc-lab-dev-Policy" {
  name        = "tc-lab-dev-Policy"
  description = "tc-lab-dev-Policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach1" {
  role       = "${aws_iam_role.tc-lab-dev.name}"
  policy_arn = "${aws_iam_policy.tc-lab-dev-Policy.arn}"
}


resource "aws_iam_role" "LambdaLibraryExecutionRole" {
  name = "LambdaLibraryExecutionRole"

  assume_role_policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "AWS": "${aws_iam_role.tc-lab-dev.arn}",
            "Service" : "lambda.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}

resource "aws_iam_policy" "LambdaLibraryExecutionPolicy" {
  name        = "LambdaLibraryExecutionPolicy"
  description = "LambdaLibraryExecutionPolicy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "VisualEditor0",
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:HeadBucket",
        "s3:GetBucketLocation"
      ],
        "Resource": "*"
    },
    {
      "Sid" : "AllowListAccessFictionFolder",
      "Effect" : "Allow",
      "Action" : "s3:ListBucket",
      "Resource" : "${aws_s3_bucket.s3library.arn}"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach2" {
  role       = "${aws_iam_role.LambdaLibraryExecutionRole.name}"
  policy_arn = "${aws_iam_policy.LambdaLibraryExecutionPolicy.arn}"
}

resource "aws_iam_role" "S3LibraryWriteRole" {
  name = "S3LibraryWriteRole"

  assume_role_policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "AWS": "${aws_iam_role.tc-lab-dev.arn}",
            "Service" : "lambda.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}

resource "aws_iam_policy" "S3LibraryWritePolicy" {
  name        = "S3LibraryWritePolicy"
  description = "S3LibraryWritePolicy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject",
        "s3:DeleteObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.s3library.arn}/fiction/",
        "${aws_s3_bucket.s3library.arn}/fiction/*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach3" {
  role       = "${aws_iam_role.S3LibraryWriteRole.name}"
  policy_arn = "${aws_iam_policy.S3LibraryWritePolicy.arn}"
}
