
data "archive_file" "lambda_zip" {
    type          = "zip"
    source_file   = "code/index.js"
    output_path   = "lambda_function.zip"
}

resource "aws_lambda_function" "LibraryLambda" {
  filename      = "lambda_function.zip"
  function_name = "librarylambda"
  role          = "${aws_iam_role.LambdaLibraryExecutionRole.arn}"
  handler       = "index.handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = "${filebase64sha256("lambda_function.zip")}"

  runtime = "nodejs8.10"
  tags = {
    Name = "Angel Castro"
    Manager = "Alex Aragon"
    Market = "DC"
    EngagementOffice = "DC"
    Email = "angel.castrobasurto@slalom.com"
  }
}
