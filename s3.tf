resource "aws_s3_bucket" "s3library" {
  bucket = "${var.s3_bucket_name}"
  acl    = "private"

  tags = {
    Name        = "${var.s3_bucket_name}"
    Environment = "Dev"
    Manager = "Alex Aragon"
    Market = "DC"
    EngagementOffice = "DC"
    Email = "angel.castrobasurto@slalom.com"
  }
}

resource "aws_s3_bucket_object" "non-fiction" {
    bucket = "${aws_s3_bucket.s3library.id}"
    acl    = "private"
    key    = "non-fiction/"
    source = "/dev/null"
}

resource "aws_s3_bucket_object" "fiction" {
    bucket = "${aws_s3_bucket.s3library.id}"
    acl    = "private"
    key    = "fiction/"
    source = "/dev/null"
}

# Bucket Objects, fiction books
resource "aws_s3_bucket_object" "TheGreatGatsby" {
    bucket = "${aws_s3_bucket.s3library.id}"
    acl    = "private"
    key    = "fiction/TheGreatGatsby"
    source = "data/fiction/TheGreatGatsby"
}
resource "aws_s3_bucket_object" "Hamlet" {
    bucket = "${aws_s3_bucket.s3library.id}"
    acl    = "private"
    key    = "fiction/Hamlet"
    source = "data/fiction/Hamlet"
}
resource "aws_s3_bucket_object" "MobyDick" {
    bucket = "${aws_s3_bucket.s3library.id}"
    acl    = "private"
    key    = "fiction/MobyDick"
    source = "data/fiction/MobyDick"
}
resource "aws_s3_bucket_object" "DonQuixote" {
    bucket = "${aws_s3_bucket.s3library.id}"
    acl    = "private"
    key    = "fiction/DonQuixote"
    source = "data/fiction/DonQuixote"
}

resource "aws_s3_bucket_policy" "S3BucketLibraryPolicy" {
  bucket = "${aws_s3_bucket.s3library.id}"

  policy = <<EOF
{
    "Id": "Policy1570732304780",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid" : "DenyAllExceptRole",
            "Effect" : "Deny",
            "Principal" : "*",
            "Action" : "s3:*",
            "Resource" : [ 
                "${aws_s3_bucket.s3library.arn}",
                "${aws_s3_bucket.s3library.arn}/*"
            ],
            "Condition" : {
                "StringNotLike" : {
                    "aws:userId": [
                        "${aws_iam_role.LambdaLibraryExecutionRole.unique_id}:*",
                        "${data.aws_caller_identity.current.user_id}",
                        "${data.aws_caller_identity.current.account_id}"
                    ] 
                }
            }
        }
    ]
}
EOF
}