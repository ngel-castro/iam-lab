# IAM & S3 policies.

Within this repository you will find how to deploy IAM, S3 bucket Policy, and a lambda function to get yourself immerse on permission concepts on AWS. Is intended to dive-in on how the S3 bucket policies and IAM policies interact each other. Also this talks about IAM roles assumption concepts and trusted relationships between roles and users/groups.

## Getting Started

Without any further ado, the following list of software is needed to deploy the resources. Please take in considerantion, since we are going to be creating AWS resources, you should have an AWS account already set up and an IAM user with Administratior permissions, or at least with IAM, S3Bucket, and Lambda function full access.

### Prerequisites

The list of software needed.

* Git
* Terraform
* Python
* AWS Cli


### Installing

AWS CLI will be needed to be configured on your machine where you are planning to deploy, Terraform uses those AWS crendetials and provision the resources.

If you haven't set up your AWS CLI Credentials on your machine please follow the next official documentation from AWS - https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html, How to set up your crendentials here : https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html.

Clone the repository from bitbucket

```
git clone https://ngel-castro@bitbucket.org/ngel-castro/iam-lab.git
```

Execute initialization command on terraform, `init` command is used to initialize a working directory containing Terraform configuration files. 

```
Terraform init
```

Look for the var.tf file to set the S3Bucket name using the *s3_bucket_name* variable.

```
variable "s3_bucket_name" {
  type = "string"
  default = "MyBucketName"
}
```
*If needed, for the LambdaFunction assumption concepts, change the the bucket name in the lambda function code inside "code" directory.*

**code/index.js:7**

```
listAllObjectsFromS3BucketFiction('MyBucketName', "**s3:prefix**");
```

Review terraform `plan` of AWS resources about to be created.

```
Terraform plan
```

If there no errors you can proceed with the actual resource provition.

```
Terraform apply
```

Write "yes" to continue.


## Built With

* [Terraform](https://www.terraform.io/) - Infrastructure As A Code to provision and manage any cloud, infrastructure or code
* [Amazon Web Services](https://aws.amazon.com/) - Cloud Provider



## Authors

* **Angel Castro**
* **Jesse Gingler**
* **Andy Losey**



