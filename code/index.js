const AWS = require('aws-sdk');
const s3 = new AWS.S3();

exports.handler = (event, context, callback) => {
    // TODO implement
    callback(null, 'Hello from Lambda');
    listAllObjectsFromS3BucketFiction('s3bucketlibrary-210', "fiction");
  };

async function listAllObjectsFromS3BucketFiction(bucket, prefix) {
    let isTruncated = true;
    let marker;
    while(isTruncated) {
      let params = { Bucket: bucket };
      if (prefix) params.Prefix = prefix;
      if (marker) params.Marker = marker;
      try {
        const response = await s3.listObjects(params).promise();
        response.Contents.forEach(item => {
          console.log(item.Key);
        });
        isTruncated = response.IsTruncated;
        if (isTruncated) {
          marker = response.Contents.slice(-1)[0].Key;
        }
    } catch(error) {
        throw error;
      }
    }
  }

